#!/usr/bin/env python3

import os
from pathlib import Path

TRAIN_DIR='train'
SOURCE_PATHS = [path for path in Path(TRAIN_DIR).glob('*') if path.is_dir()]
LABELS = [os.path.basename(path) for path in SOURCE_PATHS]
print(f'******* Labels ({len(LABELS)}): {" ".join(LABELS)}')

from imageai.Prediction.Custom import ModelTraining

BATCH_SIZE=8
MODEL_TYPES=["SqueezeNet", "DenseNet", "ResNet", "InceptionV3"]

for model_type in MODEL_TYPES:
    print(f"====== {model_type} [{BATCH_SIZE}] ======")
    model_trainer = ModelTraining()
    model_trainer.__getattribute__(f"setModelTypeAs{model_type}")()
    model_trainer.setDataDirectory(".",
        models_subdirectory=f"models-{model_type}-{BATCH_SIZE}",
        json_subdirectory=f"models-{model_type}-{BATCH_SIZE}")
    model_trainer.trainModel(
        num_objects=len(LABELS),
        num_experiments=25,
        enhance_data=True,
        batch_size=16,
        show_network_summary=True,
        save_full_model=True,
    )
