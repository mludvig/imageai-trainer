# ImageAI custom model trainer

1. **Store / sort** all your training images to *master/{label1}*, *master/{label2}*, etc.

2. Run **populate-test-train.py** - to populate *train/** and *test/** directories.

   - Takes no arguments and looks into *master/* for subdirectories (aka labels, categories).

   - Counts the number of files in each category and uses 80% of the files in the smallest
     dataset for training. The rest is used for validation / testing.

     I.e. if *label1* has 500 images and *label2* has only 100 images it will use a random
     sample of 80 images from each label from each category for testing.

   - Hard-links the training and validation datasets to *train/{labels...}* and *test/{labels...}*
     respectively.

3. Configure **run-trainer.py** to your needs

   - set the required model type (ResNet, DenseNet, etc)

   - *num_objects* must be the number of categories (subdirectories) in *train/**

   - Refer to [ImageAI Custom Model Training](https://github.com/OlafenwaMoses/ImageAI/blob/master/imageai/Prediction/CUSTOMTRAINING.md)
     for more info and options.

4. Use the trained model from *models/model_ex-NNN_acc-0.XXXX.h5* and the labels JSON file from *json/model_class.json*
   with [ImageAI Custom Model Prediction](https://github.com/OlafenwaMoses/ImageAI/blob/master/imageai/Prediction/CUSTOMPREDICTION.md#custompredictionfullmodel)
   or use our simple [**HTTP-based ImageAI service**](https://gitlab.com/mludvig/imageai-service/) server with *--custom-model* and *--custom-json* parameters.

By *Michael Ludvig*
