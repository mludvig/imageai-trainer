#!/usr/bin/env python3

import os
import sys
import random
from pathlib import Path

SOURCE='master'
TRAIN='train'
TEST='test'

TEST_PCT=20
BATCH_SIZE=8

SOURCE_PATHS = [path for path in Path('master').glob('*') if path.is_dir()]

def link_files(files, dest_dir):
    try:
        os.makedirs(dest_dir)
    except FileExistsError:
        pass
    for src_file in files:
        dest_file = dest_dir / os.path.basename(src_file)
        print(f'{src_file} -> {dest_dir}')
        os.link(src_file, dest_file)
    print('---')

master_files = {}
for path in SOURCE_PATHS:
    label = os.path.basename(path)
    p = Path(path)
    files = list(p.glob('*.jpg'))
    random.shuffle(files)
    master_files[path] = files

limit_files = min([len(x) for x in master_files.values()])
training_length = int(min(len(files), limit_files) * (100-TEST_PCT) / 100)

if training_length % BATCH_SIZE > 0:
    # Round up to BATCH_SIZE multiple
    training_length -= training_length % BATCH_SIZE
    training_length += BATCH_SIZE

for path in SOURCE_PATHS:
    files = master_files[path]
    print(f"* {path} {training_length}:{len(files)-training_length}")

    link_files(files[:training_length], Path(TRAIN) / os.path.basename(path))
    link_files(files[training_length:], Path(TEST) / os.path.basename(path))
